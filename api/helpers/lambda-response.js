/* CORS */
const ALLOWED_HEADERS = {
  "Access-Control-Allow-Headers": [
    "Content-Type",
    "Authorization",
    "X-Amz-Date",
    "X-Api-Key",
    "X-Amz-Security-Token",
    "x-amzn-RequestId",
    "X-Amz-Cf-Id",
    "x-client-id",
    "x-device-id"
  ].join(","),
  "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Credentials": true
};

class LambdaResponse {
  constructor(statusCode, message, isBase64Encoded = false) {
    let response = {
      statusCode,
      headers: ALLOWED_HEADERS,
      body: JSON.stringify({ message }),
      isBase64Encoded
    };

    console.log(`lambda response`, response)

    return response;
  }
}

module.exports = LambdaResponse;

// {
//   "errorMessage": "Expected params.Item['UserId'].N to be a string",
//   "errorType": "Error",
//   "stackTrace": [
//     "user.save.then.catch.err (/var/task/index.js:24:17)",
//     "<anonymous>",
//     "runMicrotasksCallback (internal/process/next_tick.js:121:5)",
//     "_combinedTickCallback (internal/process/next_tick.js:131:7)",
//     "process._tickDomainCallback (internal/process/next_tick.js:218:9)"
//   ]
// }
