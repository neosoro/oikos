const LambdaResponse = require("./helpers/lambda-response");
const LoginModel = require("./LoginModel");
const SwipeService = require("./services/SwipeService");

class LoginController {
  constructor(method) {
    this.method = method;
    this.LoginModel = new LoginModel();
    this.SwipeService = new SwipeService();
  }

  async auth(userEmail) {
    console.log(`LoginController.auth(${userEmail})`);

    const user = await this.LoginModel.getUserByEmail(userEmail);

    if (Object.keys(user).length == 0) {
      return new LambdaResponse(404, "Usuário não encontrado");
    } else {
      console.log(`user`, user);
      const account = await this.SwipeService.getAccount(
        user.Item.UserSwipeAccountId.S
      );

      const matchedUser = {
        UserId: user.Item.UserId.S,
        UserName: user.Item.UserName.S,
        UserEmail: user.Item.UserEmail.S,
        UserSwipeAccountId: user.Item.UserSwipeAccountId.S,
        UserBalance: account.value.balances,
        UserTags: account.value.tags
      };

      console.log(`matchedUser`, matchedUser);

      return new LambdaResponse(200, matchedUser);
    }
  }
}

module.exports = LoginController;
