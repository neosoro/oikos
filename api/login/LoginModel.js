const AWS = require("aws-sdk");

class LoginModel {
  constructor() {
    AWS.config.update({ region: "us-east-1" });
    this.DDB = new AWS.DynamoDB({ apiVersion: "2012-08-10" });
  }

  async getUserByEmail(userEmail) {
    console.log(`LoginModel.getUserByEmail(${userEmail})`);

    let user = {};

    var params = {
      TableName: "OikosUsers",
      Key: {
        UserEmail: { S: userEmail }
      }
    };
    console.log(`LoginModel.getUserByEmail()`, params);

    return new Promise((resolve, reject) => {
      this.DDB.getItem(params, function(err, data) {
        if (err) {
            console.log(`Erro ao buscar usuário`, err)
            reject(err)
        } else resolve(data);
      });
    });
  }
}

module.exports = LoginModel;
