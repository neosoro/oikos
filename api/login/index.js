const Login = require("./LoginController");
const LambdaResponse = require("./helpers/lambda-response");

exports.handler = async event => {
  console.log(`Oikos:Login:EVENT\n`, JSON.stringify(event, null, 2));

  try {
    const { httpMethod, body } = event;

    let login = new Login();

    if (httpMethod == "POST") {
      const { userEmail, userPassword } = JSON.parse(body);

      return login
        .auth(userEmail, userPassword)
        .then(response => {
          console.log(
            `LoginController.login().response`,
            response
          );
          return response;
        })
        .catch(err => {
          console.log(
            `LoginController.login().error`,
            err
          );
          return err;
        });
    }
  } catch (err) {
    console.log(`Oikos:Login:ERROR\n`, JSON.stringify(err, null, 2));
    return new LambdaResponse(500, err.message);
  }
};
