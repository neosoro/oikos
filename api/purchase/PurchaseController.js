const LambdaResponse = require("./helpers/lambda-response");
const PurchaseModel = require("./PurchaseModel");
const SwipeService = require("./services/SwipeService");

class PurchaseController {
  constructor() {
    this.PurchaseModel = new PurchaseModel();
    this.SwipeService = new SwipeService();
  }

  async save(
    userEmail,
    organizationSwipeId,
    userAccountSwipeId,
    assetType,
    assetId,
    itemId,
    itemValue,
    itemName
  ) {
    console.log(
      `PurchaseController.save(${[
        userEmail,
        organizationSwipeId,
        userAccountSwipeId,
        assetType,
        assetId,
        itemId,
        itemValue,
        itemName
      ].join(",")})`
    );

    try {
      let transfer = await this.SwipeService.transferAccountAsset(
        userAccountSwipeId,
        organizationSwipeId,
        assetId,
        itemValue
      );

      const purchased = await this.PurchaseModel.saveUserPurchase(
        transfer.data.value.id,
        userEmail,
        organizationSwipeId,
        userAccountSwipeId,
        assetType,
        assetId,
        itemId,
        itemValue,
        itemName,
        Date.now()
      );
      console.log(`this.PurchaseModel.saveUserPurchase`, purchased);
      if (!purchased) {
        throw new Error(response.message);
      }
      return new LambdaResponse(200, "Compra realizada com sucesso");
    } catch (error) {
      console.log(`this.SwipeService.makeTransfers.error`, error);
      throw new Error(error.message);
    }
  }
}

module.exports = PurchaseController;
