const AWS = require("aws-sdk");
const uuid = require("uuid");

class PurchaseModel {
  constructor() {
    AWS.config.update({ region: "us-east-1" });
    this.DDB = new AWS.DynamoDB({ apiVersion: "2012-08-10" });
  }

  async saveUserPurchase(
    transferId,
    userEmail,
    organizationSwipeId,
    userAccountSwipeId,
    assetType,
    assetId,
    itemId,
    itemValue,
    itemName,
    createdAt
  ) {
    let params = {
      TableName: "OikosUserPurchases",
      Item: {
        PurchaseId: { S: uuid.v4() },
        TransferId: {S: transferId },
        UserEmail: { S: userEmail },
        OrganizationSwipeId: { S: organizationSwipeId },
        UserAccountSwipeId: { S: userAccountSwipeId },
        AssetType: { S: assetType },
        AssetId: { S: String(assetId) },
        ItemId: { S: String(itemId) },
        ItemValue: { S: String(itemValue) },
        ItemName: { S: String(itemName) },
        CreatedAt: { S: String(createdAt) }
      }
    };
    console.log(`PurchaseModel.saveUserPurchase()`, params);
    return new Promise((resolve, reject) => {
      this.DDB.putItem(params, function(err, data) {
        if (err) reject(err);
        else resolve(true);
      });
    });
  }
}

module.exports = PurchaseModel;
