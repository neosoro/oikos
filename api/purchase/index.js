const Purchase = require("./PurchaseController");
const LambdaResponse = require("./helpers/lambda-response");

exports.handler = async event => {
  console.log(`Oikos:Purchase:EVENT\n`, JSON.stringify(event, null, 2));

  try {
    const { path, httpMethod, body } = event;

    let purchase = new Purchase();

    if (httpMethod == "POST" && path == "/purchase") {
      const {
        userEmail,
        organizationSwipeId,
        userAccountSwipeId,
        assetType,
        assetId,
        itemId,
        itemValue,
        itemName
      } = JSON.parse(body);

      return purchase
        .save(
          userEmail,
          organizationSwipeId,
          userAccountSwipeId,
          assetType,
          assetId,
          itemId,
          itemValue,
          itemName
        )
        .then(response => {
          console.log(`PurchaseController.save().response`, response);
          return response;
        })
        .catch(err => {
          console.log(`PurchaseController.save().error`, err);
          return new LambdaResponse(400, err.message);
        });
    }
  } catch (err) {
    console.log(`Oikos:ERROR\n`, JSON.stringify(err, null, 2));
    return new LambdaResponse(500, err.message);
  }
};
