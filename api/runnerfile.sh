PROJECT="starpay-oikos"
AWS_ACCOUNT_ID="168003807716"
AWS_REGION="us-east-1"
AWS_PROFILE="default"

task_deploy () {
  if [[ $# -eq 0 ]]; then
    runner_log_error "missing stage option: --production || --dev"
    runner_log_error "missing resource option: --user --login --purchase"
    return 1
  fi

  local TYPE=${1/--}
  local RESOURCE=${2/--}

  runner_log_success "Iniciando deploy de aplicação $TYPE"
  if [[ "$TYPE" == "dev" ]]; then
    mkdir -p _build/helpers/; cp -R helpers/* _build/helpers/
    mkdir -p _build/services/; cp -R services/* _build/services/
    mkdir -p _build/node_modules/; cp -R node_modules/* _build/node_modules/
    cp $RESOURCE/* _build
    cd _build/
    ZIP_NAME=lambda.zip
    zip -r $ZIP_NAME .

    LAMBDA_ARN=arn:aws:lambda:us-east-1:168003807716:function:oikos-$RESOURCE-api
    AWS_PROFILE=$AWS_PROFILE
    AWS_REGION=$AWS_REGION
    ZIP_DEST=fileb://lambda.zip
    rm -rf _build/

    runner_log_success "Iniciando atualização de lambda..."
    aws lambda update-function-code --region $AWS_REGION --zip-file $ZIP_DEST --function-name $LAMBDA_ARN --profile $AWS_PROFILE
    runner_log_success "Lambda atualizada com sucesso..."
    rm -rf lambda.zip
  fi

  runner_log_success "Fim task_deploy"
}