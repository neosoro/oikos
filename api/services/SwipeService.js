const LambdaResponse = require("../helpers/lambda-response");
const Swipe = require("@swp/swipe-sdk");

class SwipeService {
  constructor() {
    this.swipe = Swipe.init({
      apiKey:
        "4325b618ea4eca4d059a2dcb586f18d6d480aefcc810866084b3df407adcd7da",
      secret:
        "4524c2f308ce6c3b9a70b2964ce3adab61adb9530b3ebcad50b980e121c203bd",
      language: Swipe.languages.EN_US,
      sandbox: true,
      debug: true
    });
  }

  async listRedeemedAssets(accountId) {
    return this.swipe.getAllTransfers(accountId)
  }

  async transferAccountAsset(from, to, assetId, assetAmount) {
    console.log(`Tranfering values between accounts`);

    try {
      return this.swipe.makeTransfers({
        actions: [
          {
            from,
            to,
            asset: assetId,
            amount: assetAmount
          }
        ],
        memo: Date.now()
      });
    } catch (error) {
      console.log(`error`, error);
    }
  }

  async createAccount() {
    console.log(`async createAccount()`);
    return await this.swipe.createAccount();
  }

  async getAccount(accountId) {
    return this.swipe
      .getAccount(accountId)
      .then(({ data }) => {
        console.log(`async getAccount(accountId).success`, data);
        return data;
      })
      .catch(err => {
        console.log(`async getAccount(accountId).err`, err);
        return err;
      });
  }

  async getOrganization() {
    return this.swipe
      .getOrganization()
      .then(({ data }) => {
        console.log(
          `this.SwipeService.getOrganization - OK`,
          JSON.stringify(data, null, 2)
        );
        return new LambdaResponse(200, data);
      })
      .catch(error => {
        console.log(
          `this.SwipeService.getOrganization - error`,
          JSON.stringify(error, null, 2)
        );
        throw new Error(error.message);
      });
  }
}

module.exports = SwipeService;
