const LambdaResponse = require("./helpers/lambda-response");
const UserModel = require("./UserModel");
const SwipeService = require("./services/SwipeService");

class UserController {
  constructor() {
    this.UserModel = new UserModel();
    this.SwipeService = new SwipeService();
  }

  async listRedeem(accountId) {
    let transfers = await this.SwipeService.listRedeemedAssets(accountId)
    
    return new LambdaResponse(200, transfers.data);
  }

  async redeem(
    userEmail,
    organizationSwipeId,
    userAccountSwipeId,
    assetAmount,
    assetValue,
    assetType,
    assetQtd,
    assetId
  ) {
    console.log(
      `UserController.redeem(${[
        userEmail,
        organizationSwipeId,
        userAccountSwipeId,
        assetAmount,
        assetValue,
        assetType,
        assetQtd,
        assetId
      ].join(",")})`
    );

    try {
      let transfer = await this.SwipeService.transferAccountAsset(
        organizationSwipeId,
        userAccountSwipeId,
        assetId,
        assetAmount
      );

      console.log(`this.SwipeService.transferAccountAsset.transfer`, transfer);

      const redeemed = await this.UserModel.redeemUserAsset(
        userEmail,
        organizationSwipeId,
        userAccountSwipeId,
        transfer.data.value.id,
        assetAmount,
        assetValue,
        assetType,
        assetQtd,
        assetId,
        Date.now()
      );
      console.log(`this.UserModel.redeemUserAsset`, redeemed);
      if (!redeemed) {
        throw new Error(response.message);
      }

      return new LambdaResponse(200, "Resgate realizado com sucesso");
    } catch (error) {
      console.log(`this.SwipeService.makeTransfers.error`, error);
      throw new Error(error.message);
    }
  }

  async find(accountId, userEmail) {
    console.log(`UserController.find(${userEmail})`);
    const account = await this.SwipeService.getAccount(accountId);
    const user = await this.UserModel.getUserByEmail(userEmail);

    console.log(`account`, account);
    console.log(`user`, user);

    if (Object.keys(user).length == 0) {
      return new LambdaResponse(404, "Usuário não encontrado");
    } else {
      const userProfile = {
        UserId: user.Item.UserId.S,
        UserName: user.Item.UserName.S,
        UserEmail: user.Item.UserEmail.S,
        UserBalance: account.value.balances,
        UserTags: account.value.tags
      };

      console.log(`userProfile`, userProfile);

      return new LambdaResponse(200, userProfile);
    }
  }

  async save(userEmail, userPassword, userName) {
    const acc = await this.SwipeService.createAccount();
    console.log(`SwipeAccount`, acc);

    return this.UserModel.save(
      userEmail,
      userPassword,
      userName,
      acc.data.value.id
    )
      .then(data => {
        console.log(
          `this.UserModel.save.response`,
          JSON.stringify(data, null, 2)
        );
        return new LambdaResponse(201, "Usuário criado com sucesso!");
      })
      .catch(error => {
        console.log(
          `this.UserModel.save.error`,
          JSON.stringify(error, null, 2)
        );
        throw new Error(error.message);
      });
  }
}

module.exports = UserController;
