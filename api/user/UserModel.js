const AWS = require("aws-sdk");
const uuid = require("uuid");
const crypto = require("crypto");

class UserModel {
  constructor() {
    AWS.config.update({ region: "us-east-1" });
    this.DDB = new AWS.DynamoDB({ apiVersion: "2012-08-10" });
  }

  async redeemUserAsset(
    userEmail,
    organizationSwipeId,
    userAccountSwipeId,
    transferId,
    assetAmount,
    assetValue,
    assetType,
    assetQtd,
    assetId,
    createdAt
  ) {
    let params = {
      TableName: "OikosUserAssets",
      Item: {
        UserEmail: { S: userEmail }, 
        OrganizationSwipeId: { S: organizationSwipeId },
        UserAccountSwipeId: { S: userAccountSwipeId },
        TransferId: { S: transferId },
        AssetAmount: { S: String(assetAmount) },
        AssetValue: { S: String(assetValue) },
        AssetQtd: { S: String(assetQtd) },
        AssetType: { S: assetType },
        AssetId: { S: String(assetId) },
        CreatedAt: {S: String(createdAt)},
      }
    };
    console.log(`UserModel.redeemUserAsset()`, params);
    return new Promise((resolve, reject) => {
      this.DDB.putItem(params, function(err, data) {
        if (err) reject(err);
        else resolve(true);
      });
    });
  }

  async getUserBySwipeAccountId(accountId) {
    var params = {
      TableName: "OikosUsers",
      Key: {
        UserSwipeAccountId: { S: accountId }
      }
    };
    console.log(`UserModel.getUserBySwipeAccountId()`, params);

    return new Promise((resolve, reject) => {
      this.DDB.getItem(params, function(err, data) {
        if (err) reject(err);
        else resolve(data);
      });
    });
  }

  async getUserByEmail(userEmail) {
    var params = {
      TableName: "OikosUsers",
      Key: {
        UserEmail: { S: userEmail }
      }
    };
    console.log(`UserModel.getUserByEmail()`, params);

    return new Promise((resolve, reject) => {
      this.DDB.getItem(params, function(err, data) {
        if (err) reject(err);
        else resolve(data);
      });
    });
  }

  async save(userEmail, userPassword, userName, userSwipeAccountId) {
    let params = {
      TableName: "OikosUsers",
      Item: {
        UserId: { S: uuid.v4() },
        UserName: { S: userName },
        UserEmail: { S: userEmail },
        UserPassword: {
          S: crypto
            .createHash("md5")
            .update(userPassword)
            .digest("hex")
        },
        UserSwipeAccountId: { S: userSwipeAccountId }
      }
    };
    console.log(`UserModel.save()`, params);
    return new Promise((resolve, reject) => {
      this.DDB.putItem(params, function(err, data) {
        if (err) reject(err);
        else resolve(data);
      });
    });
  }
}

module.exports = UserModel;
