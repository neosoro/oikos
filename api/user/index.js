const User = require("./UserController");
const LambdaResponse = require("./helpers/lambda-response");

exports.handler = async event => {
  console.log(`Oikos:EVENT\n`, JSON.stringify(event, null, 2));

  try {
    const { path, httpMethod, body, queryStringParameters } = event;

    let user = new User();

    if (path == "/redeem" && httpMethod == "GET") {
      return user
        .listRedeem(queryStringParameters.accountId)
        .then(response => {
          console.log(`UserController.listRedeem().response`, response);
          return response;
        })
        .catch(err => {
          console.log(`UserController.listRedeem().error`, err);
          return new LambdaResponse(500, err.message);
        });
    }

    if (
      httpMethod == "GET" &&
      (queryStringParameters.hasOwnProperty("accountId") &&
        queryStringParameters.hasOwnProperty("userEmail"))
    ) {
      console.log(`queryStringParameters`, queryStringParameters);
      return user
        .find(queryStringParameters.accountId, queryStringParameters.userEmail)
        .then(response => {
          console.log(`UserController.find().response`, response);
          return response;
        })
        .catch(err => {
          console.log(`UserController.find().error`, err);
          return new LambdaResponse(500, err.message);
        });
    }

    if (httpMethod == "POST" && path == "/redeem") {
      const {
        userEmail,
        organizationSwipeId,
        userAccountSwipeId,
        assetAmount,
        assetValue,
        assetType,
        assetQtd,
        assetId
      } = JSON.parse(body);

      return user
        .redeem(
          userEmail,
          organizationSwipeId,
          userAccountSwipeId,
          assetAmount,
          assetValue,
          assetType,
          assetQtd,
          assetId
        )
        .then(response => {
          console.log(`UserController.redeem().response`, response);
          return response;
        })
        .catch(err => {
          console.log(`UserController.redeem().error`, err);
          return new LambdaResponse(400, err.message);
        });
    }

    if (httpMethod == "POST") {
      const { userEmail, userPassword, userName } = JSON.parse(body);

      return user
        .save(userEmail, userPassword, userName)
        .then(response => {
          console.log(`UserController.save().response`, response);
          return response;
        })
        .catch(err => {
          console.log(`UserController.save().error`, err);
          return err;
        });
    }
  } catch (err) {
    console.log(`Oikos:ERROR\n`, JSON.stringify(err, null, 2));
    return new LambdaResponse(500, err.message);
  }
};
