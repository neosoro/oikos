import React, { Component } from "react";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import axios from "axios";
import { Redirect } from "react-router-dom";
import store from "store";
import md5 from "md5";

class Login extends Component {
  constructor(props) {
    super(props);

    this.axios = axios.create({
      baseURL: "https://8c6bvs4qqh.execute-api.us-east-1.amazonaws.com/dev"
    });

    this.state = {
      redirectToHome: false,
      userEmail: "",
      userPassword: "",
      alertShow: false,
      variant: "",
      loginMessage: "",
      loggedIn: false
    };
  }

  handleFormChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  renderHomeRedirect = () => {
    console.log(`this.state.redirectToHome`, this.state.redirectToHome)
    if (this.state.redirectToHome) {
      return <Redirect to="/dashboard/" />;
    }
  };

  handleFormLogin = e => {
    e.preventDefault();

    this.axios
      .post("/login", {
        userEmail: this.state.userEmail,
        userPassword: md5(this.state.userPassword)
      })
      .then(response => {
        console.log(`OK`, response);

        let user = response.data.message;

        store.set("loggedIn", true);
        store.set("userAccountSwipeId", user.UserSwipeAccountId);
        store.set("userEmail", user.UserEmail);
        store.set("userName", user.UserName);
        store.set("userId", user.UserId);
        store.set("userBalance", user.UserBalance);
        store.set("userTags", user.UserTags);

        this.setState({ redirectToHome: true });
      })
      .catch(error => {
        console.log(`NOK`, error);

        store.set("loggedIn", false);

        this.setState({
          alertShow: true,
          variant: "danger",
          loginMessage: error.message,
          loggedIn: false,
          redirectToHome: false
        });
      });
  };

  handleAlertDismiss = () => {
    this.setState({
      userEmail: "",
      userPassword: "",
      loginMessage: "",
      alertShow: false,
      variant: ""
    });
  };

  handleAlert = () => {
    const { handleAlertDismiss } = this;

    if (this.state.alertShow) {
      return (
        <Alert
          variant={this.state.variant}
          onClose={handleAlertDismiss}
          dismissible
        >
          <Alert.Heading>{this.state.loginMessage}</Alert.Heading>
        </Alert>
      );
    }
  };

  render() {
    const {
      handleFormLogin,
      handleFormChange,
      handleAlert,
      renderHomeRedirect
    } = this;

    return (
      <Container>
        {renderHomeRedirect()}
        <Row>
          <Col>{handleAlert()}</Col>
        </Row>
        <Row>
          <Col>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  name="userEmail"
                  value={this.state.userEmail}
                  onChange={e => handleFormChange(e)}
                  placeholder="Enter email"
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="userPassword"
                  value={this.state.userPassword}
                  onChange={e => handleFormChange(e)}
                  placeholder="Password"
                />
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={e => handleFormLogin(e)}
              >
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Login;
