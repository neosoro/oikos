import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import axios from "axios";
import store from "store";
import CurrencyFormat from "react-currency-format";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.axios = axios.create({
      baseURL: "https://8c6bvs4qqh.execute-api.us-east-1.amazonaws.com/dev"
    });

    this.state = {
      UserSwipeAccountId: store.get("userAccountSwipeId"),
      UserName: store.get("userName"),
      UserEmail: store.get("userEmail"),
      UserBalance: store.get("userBalance"),
      UserTags: store.get("userTags"),
      UserTotalCredit: 0
    };
  }

  async componentDidMount() {
    const response = await this.axios.get(`/user`, {
      params: {
        accountId: this.state.UserSwipeAccountId,
        userEmail: this.state.UserEmail
      }
    });

    let user = response.data.message;

    let totalCredit = user.UserBalance.reduce((acc, balance, i) => {
      return Number(acc) + Number(balance.balance);
    }, 0);

    store.set(
      "userBalanceByAsset",
      user.UserBalance.map(balance => {
        return {
          assetType: balance.asset_code,
          assetTotal: balance.balance
        };
      })
    );

    this.setState({
      UserId: user.UserId,
      UserName: user.UserName,
      UserEmail: user.UserEmail,
      UserBalance: user.UserBalance,
      UserTags: user.UserTags,
      UserTotalCredit: totalCredit
    });
  }

  render() {
    return (
      <Container>
        <p>{this.state.UserName}</p>
        <p>{this.state.UserEmail}</p>
        <Row>
          <Col>
            <h3>Ativos</h3>
            {this.state.UserBalance.map((balance, i) => {
              return (
                <div key={i}>
                  <h6>{balance.asset_code}</h6>
                  <p>
                    R${" "}
                    {Number(balance.balance)
                      .toFixed(2)
                      .replace(".", ",")}
                  </p>
                </div>
              );
            })}
          </Col>
        </Row>
        <Row>
          <Col>
            <h3>Saldo Total</h3>
            <p>
              R$
              <CurrencyFormat
                value={Number(this.state.UserTotalCredit).toFixed(2)}
                displayType={"text"}
                decimalSeparator="."
                thousandSeparator=","
              />
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Dashboard;
