import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Logo from "../logo.png";

const LogoStyle = {
  width: "100px",
  height: "100px",
  display: "block",
  marginRight: "auto",
  marginLeft: "auto"
};

const CTAText = {
  textAlign: "center"
};

class Home extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col>
            <img alt="Logo" src={Logo} style={LogoStyle} />
          </Col>
        </Row>
        <Row>
          <Col>
            <h1 style={CTAText}>Oikos</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <h5 style={CTAText}>
              OIKOS, prefixo da palavra οικολογία (Ecologia) vem do Grego, que
              significa casa.
            </h5>
            <h6 style={CTAText}>
              OIKOS é uma eco moeda que recompensa seus usuários, por material
              reciclável recolhido.
            </h6>
            <h6 style={CTAText}>
              Tenha acesso a descontos exclusivos em nossa loja e compre com suas Oikoins!
            </h6>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;
