import React, { Component } from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import store from "store";
import axios from "axios";
import CurrencyFormat from "react-currency-format";

class ListRedeem extends Component {
  constructor(props) {
    super(props);

    this.axios = axios.create({
      baseURL: "https://8c6bvs4qqh.execute-api.us-east-1.amazonaws.com/dev"
    });

    this.state = {
      UserSwipeAccountId: store.get("userAccountSwipeId"),
      UserName: store.get("userName"),
      UserEmail: store.get("userEmail"),
      UserBalance: store.get("userBalance"),
      UserTags: store.get("userTags"),
      UserTotalCredit: 0
    };
  }

  async componentDidMount() {
    const response = await this.axios.get(`/redeem`, {
      params: {
        accountId: this.state.UserSwipeAccountId
      }
    });

    console.log(`response`, response.data.message);

    this.setState({
      transfers: response.data.message
    });
  }

  renderRewardRedeemList = ({ receipt, value }, i) => {
    console.log(`receipt, value`, receipt, value);
    return (
      <tr key={i}>
        <td>{i+1}</td>
        <td>{receipt.created_at}</td>
        <td>{Number(value.amount).toFixed(2)}</td>
      </tr>
    );
  };

  render() {
    const { renderRewardRedeemList } = this;
    console.log(`this.state.transfers`, this.state.transfers);
    return (
      <Container>
        <Row>
          <Col>
            <h3>Resgates</h3>
          </Col>

          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Valor</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody>
              {this.state.transfers &&
                this.state.transfers.map(renderRewardRedeemList)}
            </tbody>
          </Table>
        </Row>
      </Container>
    );
  }
}

export default ListRedeem;
