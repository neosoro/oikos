import React, { Component } from "react";
import { LinkContainer } from "react-router-bootstrap";
import {
  Container,
  Row,
  Col,
  Dropdown,
  Form,
  Button,
  Alert
} from "react-bootstrap";
import axios from "axios";
import store from "store";

const FormStyle = {
  marginTop: "10px"
};

const ButtonListStyle = {
  margin: "10px"
};

const Assets = {
  METAL: {
    assetType: "METAL",
    assetId: "0f3e3a5a2e5adc84b6d0416a0494eb6f05d7f8755cfaad297a6422fcc44e54e1",
    assetValue: 1.9
  },
  PLASTICO: {
    assetType: "PLASTICO",
    assetId: "971d2717f35fd9a712383bc745f77c973bf14cbbc57a1774310fa2b25ba1122e",
    assetValue: 0.9
  },
  VIDRO: {
    assetType: "VIDRO",
    assetId: "fa7379476a74d2a27c95741a9822591b9c35503d0550eafe3b4cf63d2c0f2657",
    assetValue: 2.7
  },
  PAPEL: {
    assetType: "PAPEL",
    assetId: "4396d9cd9b67f9eec6d1eb4926eb3317fcfe1d02f7b52bc0ba94510457f94764",
    assetValue: 1.9
  }
};

class Redeem extends Component {
  constructor(props) {
    super(props);

    this.axios = axios.create({
      baseURL: "https://8c6bvs4qqh.execute-api.us-east-1.amazonaws.com/dev"
    });

    this.state = {
      assetQtd: 0,
      assetValue: 0,
      assetTotalValue: 0,
      assetType: "",
      assetId: "",
      alertShow: false,
      variant: "",
      redeemMessage: ""
    };
  }

  handleAssetMaterial = (eventKeyValue, event) => {
    let asset = Assets[eventKeyValue];

    this.setState({
      assetType: asset.assetType,
      assetId: asset.assetId,
      assetValue: asset.assetValue
    });
  };

  handleAssetQtdChange = e => {
    let assetTotalValue = e.target.value * this.state.assetValue;

    this.setState({
      [e.target.name]: e.target.value,
      assetTotalValue: assetTotalValue.toFixed(2)
    });
  };

  handleFormRedeem = e => {
    e.preventDefault();

    console.log(`handleFormRedeem`, {
      userEmail: store.get("userEmail"),
      organizationSwipeId:
        "9c5dff6cc0b085eb76d99d2372c5a44f00e2700220cf472c78fe479ed467439b",
      userAccountSwipeId: store.get("userAccountSwipeId"),
      assetAmount: this.state.assetTotalValue,
      assetValue: this.state.assetValue,
      assetType: this.state.assetType,
      assetQtd: this.state.assetQtd,
      assetId: this.state.assetId
    });

    this.axios
      .post("/redeem", {
        userEmail: store.get("userEmail"),
        organizationSwipeId:
          "9c5dff6cc0b085eb76d99d2372c5a44f00e2700220cf472c78fe479ed467439b",
        userAccountSwipeId: store.get("userAccountSwipeId"),
        assetAmount: this.state.assetTotalValue,
        assetValue: this.state.assetValue,
        assetType: this.state.assetType,
        assetQtd: this.state.assetQtd,
        assetId: this.state.assetId
      })
      .then(response => {
        console.log(`redeem.response`, response);
        this.setState({
          redeemMessage: response.data.message,
          alertShow: true,
          variant: "success"
        });
      })
      .catch(error => {
        console.log(`redeem.error`, error);
        this.setState({
          redeemMessage: error.message,
          alertShow: true,
          variant: "danger"
        });
      });
  };

  handleAlertDismiss = () => {
    this.setState({
      assetQtd: 0,
      assetValue: 0,
      assetTotalValue: 0,
      assetType: "",
      assetId: "",
      alertShow: false,
      variant: "",
      redeemMessage: ""
    });
  };

  handleAlert = () => {
    const { handleAlertDismiss } = this;

    if (this.state.alertShow) {
      return (
        <Alert
          variant={this.state.variant}
          onClose={handleAlertDismiss}
          dismissible
        >
          <Alert.Heading>{this.state.redeemMessage}</Alert.Heading>
        </Alert>
      );
    }
  };

  render() {
    const {
      handleAssetMaterial,
      handleFormRedeem,
      handleAssetQtdChange,
      handleAlert
    } = this;
    return (
      <Container>
        <Row>
          <Col>{handleAlert(this.state.redeemMessage)}</Col>
        </Row>
        <Row>
          <Col>
            <Form style={FormStyle}>
              <Form.Group controlId="assetMaterial">
                <Dropdown>
                  <Dropdown.Toggle variant="success" id="dropdown-basic">
                    Material
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item
                      onSelect={(eventKey, event) =>
                        handleAssetMaterial(eventKey, event)
                      }
                      eventKey="METAL"
                    >
                      Metal
                    </Dropdown.Item>
                    <Dropdown.Item
                      onSelect={(eventKey, event) =>
                        handleAssetMaterial(eventKey, event)
                      }
                      eventKey="PAPEL"
                    >
                      Papel
                    </Dropdown.Item>
                    <Dropdown.Item
                      onSelect={(eventKey, event) =>
                        handleAssetMaterial(eventKey, event)
                      }
                      eventKey="PLASTICO"
                    >
                      Plástico
                    </Dropdown.Item>
                    <Dropdown.Item
                      onSelect={(eventKey, event) =>
                        handleAssetMaterial(eventKey, event)
                      }
                      eventKey="VIDRO"
                    >
                      Vidro
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </Form.Group>

              <Form.Group controlId="assetQtd">
                <Form.Label>Quantidade</Form.Label>
                <Form.Control
                  type="text"
                  name="assetQtd"
                  value={this.state.assetQtd}
                  onChange={e => handleAssetQtdChange(e)}
                  placeholder="Quantidade"
                />
              </Form.Group>
              <Form.Group controlId="assetQtd">
                <Form.Label>Material {this.state.assetType}</Form.Label>
                <br />
                <Form.Label>Valor Base R$ {this.state.assetValue}</Form.Label>
                <br />
                <Form.Label>
                  Valor Total R$ {this.state.assetTotalValue}
                </Form.Label>
                <Form.Control
                  type="hidden"
                  name="assetValue"
                  value={this.state.assetValue}
                  placeholder="Quantidade"
                />
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                style={ButtonListStyle}
                onClick={e => handleFormRedeem(e)}
              >
                Resgatar
              </Button>
              <LinkContainer to="/list-redeem">
                <Button variant="success" style={ButtonListStyle}>Ver meus resgates</Button>
              </LinkContainer>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Redeem;
