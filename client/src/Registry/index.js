import React, { Component } from "react";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import axios from "axios";

class Registry extends Component {
  constructor(props) {
    super(props);

    this.axios = axios.create({
      baseURL: "https://8c6bvs4qqh.execute-api.us-east-1.amazonaws.com/dev"
    });

    this.state = {
      userEmail: "",
      userPassword: "",
      userName: "",
      alertShow: false,
      variant: "",
      registryMessage: ""
    };
  }

  handleFormChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleFormSave = e => {
    e.preventDefault();

    this.axios
      .post("/user", {
        userEmail: this.state.userEmail,
        userPassword: this.state.userPassword,
        userName: this.state.userName,
      })
      .then(function(response) {
        console.log(`OK`, response);
        return response;
      })
      .then(response => {
        let registryMessage = response.data.message;

        this.setState({
          alertShow: true,
          variant: "success",
          registryMessage
        });
      })
      .catch(function(error) {
        console.log(`NOK`, error);
        this.setState({
          alertShow: true,
          variant: "danger",
          registryMessage: error.message
        });
      });
  };

  handleAlertDismiss = () => {
    this.setState({
      userEmail: "",
      userPassword: "",
      userName: "",
      registryMessage: "",
      alertShow: false,
      variant: ""
    });
  };

  handleAlert = () => {
    const { handleAlertDismiss } = this;

    if (this.state.alertShow) {
      return (
        <Alert
          variant={this.state.variant}
          onClose={handleAlertDismiss}
          dismissible
        >
          <Alert.Heading>{this.state.registryMessage}</Alert.Heading>
        </Alert>
      );
    }
  };

  render() {
    const { handleFormSave, handleFormChange, handleAlert } = this;
    return (
      <Container>
        <Row>
          <Col>{handleAlert(this.state.registryMessage)}</Col>
        </Row>
        <Row>
          <Col>
            <Form>
            <Form.Group controlId="formBasicName">
                <Form.Label>Nome</Form.Label>
                <Form.Control
                  type="text"
                  name="userName"
                  value={this.state.userName}
                  onChange={e => handleFormChange(e)}
                  placeholder="Enter your name"
                />
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  name="userEmail"
                  value={this.state.userEmail}
                  onChange={e => handleFormChange(e)}
                  placeholder="Enter email"
                />
                <Form.Text className="text-muted">
                  Não se preocupe! Não compatilharemos suas informações com ninguém.
                </Form.Text>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="userPassword"
                  value={this.state.userPassword}
                  onChange={e => handleFormChange(e)}
                  placeholder="Password"
                />
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={e => handleFormSave(e)}
              >
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Registry;
