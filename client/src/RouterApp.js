import React, { Component } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import store from "store";
import Logo from "./logo.png";

import Home from "./Home";
import Redeem from "./Redeem";
import ListRedeem from "./ListRedeem";
import Registry from "./Registry";
import Login from "./Auth/login";
import Dashboard from "./Dashboard";
import Shopping from "./Shopping";

const LogoStyle = {
  width: "32px",
  height: "32px"
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: store.get("loggedIn")
    };
  }

  logOffUser = () => {
    store.clearAll();

    this.setState({
      isLoggedIn: false
    });
  };

  renderMenu = () => {
    let Menu = {};
    const { logOffUser } = this

    if (this.state.isLoggedIn) {
      Menu = (
        <Nav className="mr-auto">
          <LinkContainer to="/dashboard/">
            <Nav.Link>Dashboard</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/redeem/">
            <Nav.Link>Resgatar</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/shopping/">
            <Nav.Link>Shopping</Nav.Link>
          </LinkContainer>
          <Nav.Link onClick={() => logOffUser()}>Logout</Nav.Link>
        </Nav>
      );
    } else {
      Menu = (
        <Nav className="mr-auto">
          <LinkContainer to="/">
            <Nav.Link>Home</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/registry/">
            <Nav.Link>Cadastro</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/login/">
            <Nav.Link>Login</Nav.Link>
          </LinkContainer>
        </Nav>
      );
    }
    return Menu;
  };

  render() {
    console.log(`this.state.isLoggedIn`, this.state.isLoggedIn)
    const { renderMenu } = this;

    return (
      <Router>
        <Navbar bg="light" expand="lg">
          <LinkContainer to="/">
            <Navbar.Brand>
              <img alt="Logo" src={Logo} style={LogoStyle} />
            </Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            {renderMenu()}
          </Navbar.Collapse>
        </Navbar>

        <Route path="/" exact component={Home} />
        <Route path="/login/" component={Login} />
        <Route path="/registry/" component={Registry} />
        <Route path="/redeem/" component={Redeem} />
        <Route path="/list-redeem/" component={ListRedeem} />
        <Route path="/shopping/" component={Shopping} />
        <Route path="/dashboard/" component={Dashboard} />
      </Router>
    );
  }
}

export default App;
