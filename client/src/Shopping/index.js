import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  Alert,
  Form,
  Dropdown
} from "react-bootstrap";
import axios from "axios";
import store from "store";
import CurrencyFormat from "react-currency-format";

const Assets = {
  METAL: {
    assetType: "METAL",
    assetId: "0f3e3a5a2e5adc84b6d0416a0494eb6f05d7f8755cfaad297a6422fcc44e54e1"
  },
  PLASTICO: {
    assetType: "PLASTICO",
    assetId: "971d2717f35fd9a712383bc745f77c973bf14cbbc57a1774310fa2b25ba1122e"
  },
  VIDRO: {
    assetType: "VIDRO",
    assetId: "fa7379476a74d2a27c95741a9822591b9c35503d0550eafe3b4cf63d2c0f2657"
  },
  PAPEL: {
    assetType: "PAPEL",
    assetId: "4396d9cd9b67f9eec6d1eb4926eb3317fcfe1d02f7b52bc0ba94510457f94764"
  }
};

const CardStyle = {
  margin: "10px auto",
  width: "18rem"
};

class Shopping extends Component {
  constructor(props) {
    super(props);

    this.axios = axios.create({
      baseURL: "https://8c6bvs4qqh.execute-api.us-east-1.amazonaws.com/dev"
    });

    this.state = {
      UserSwipeAccountId: store.get("userAccountSwipeId"),
      UserName: store.get("userName"),
      UserEmail: store.get("userEmail"),
      UserBalance: store.get("userBalance"),
      UserBalanceByAsset: store.get("userBalanceByAsset"),
      UserTags: store.get("userTags"),
      UserTotalCredit: 0,
      itemPurchased: false,
      purchaseMessage: "",
      itemsForSale: [
        {
          itemId: 123,
          image:
            "https://cdn.entrypoint.directory/assets/39790/produtos/1669/oleo-de-coco-organico-unilife-200ml.jpg",
          name: "Óleo de coco",
          value: 33.59,
          description: "Óleo de coco. Sua comida vai ficar deliciosa!"
        },
        {
          itemId: 456,
          image:
            "https://lgcompras.com.br/wp-content/uploads/2017/02/407871_ovoshhi_frukty_salat_yabloki_baklazhany_perec_pomi_1680x1050_www.Gde-Fon.com_.jpg",
          name: "Cesta de legumes",
          value: 5.35,
          description: "Somente para você. 50% de desconto na compra de legumes"
        },
        {
          itemId: 789,
          image:
            "https://lgcompras.com.br/wp-content/uploads/2017/02/407871_ovoshhi_frukty_salat_yabloki_baklazhany_perec_pomi_1680x1050_www.Gde-Fon.com_.jpg",
          name: "Cesta de frutas com 40% de desconto!",
          value: 9.35,
          description: "Somente para você. 40% de desconto na compra de legumes"
        }
      ],
      alertShow: false,
      variant: "",
      shoppingMessage: ""
    };
  }

  ItemShoppingCard = ({ itemId, image, name, value, description }, index) => {
    const { handleFormShopping } = this;

    return (
      <Card key={index} style={CardStyle}>
        <Card.Img variant="top" src={image} />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>
            {description} <br />
            R$ {Number(value).toFixed(2)}
          </Card.Text>
          <Button
            variant="primary"
            onClick={e => handleFormShopping(e, itemId, value, name)}
          >
            Comprar
          </Button>
        </Card.Body>
      </Card>
    );
  };

  handleAssetMaterial = (eventKeyValue, event) => {
    let asset = Assets[eventKeyValue];

    function getAssetByType(assets, value) {
      return assets
        .map(function(e) {
          return e.assetType;
        })
        .indexOf(value);
    }

    let assetIndex = getAssetByType(
      this.state.UserBalanceByAsset,
      eventKeyValue
    );

    this.setState({
      assetType: asset.assetType,
      assetId: asset.assetId,
      assetTotal:
        (this.state.UserBalanceByAsset[assetIndex] &&
          this.state.UserBalanceByAsset[assetIndex].assetTotal) ||
        0
    });
  };

  handleConfirmPurchase = () => {
    if (this.state.assetTotal < this.state.itemValue) {
      this.setState({
        alertShow: true,
        itemPurchased: false,
        variant: "danger",
        shoppingMessage: `Seu saldo é insuficiente\nSeu saldo: R$ ${Number(
          this.state.assetTotal
        )
          .toFixed(2)
          .replace(".", ",")}`
      });
    } else {
      console.log(`handleConfirmPurchase`, {
        userEmail: store.get("userEmail"),
        organizationSwipeId:
          "9c5dff6cc0b085eb76d99d2372c5a44f00e2700220cf472c78fe479ed467439b",
        userAccountSwipeId: store.get("userAccountSwipeId"),
        assetType: this.state.assetType,
        assetId: this.state.assetId,
        itemId: this.state.itemId,
        itemValue: this.state.itemValue,
        itemName: this.state.itemName
      });

      this.axios
        .post("/purchase", {
          userEmail: store.get("userEmail"),
          organizationSwipeId:
            "9c5dff6cc0b085eb76d99d2372c5a44f00e2700220cf472c78fe479ed467439b",
          userAccountSwipeId: store.get("userAccountSwipeId"),
          assetType: this.state.assetType,
          assetId: this.state.assetId,
          itemId: this.state.itemId,
          itemValue: this.state.itemValue,
          itemName: this.state.itemName
        })
        .then(response => {
          console.log(`purchase.response`, response);
          this.setState({
            shoppingMessage: response.data.message,
            itemPurchased: false,
            alertShow: true,
            variant: "success"
          });
        })
        .catch(error => {
          console.log(`purchase.error`, error);
          this.setState({
            purchaseMessage: error.message,
            alertShow: true,
            variant: "danger"
          });
        });
    }
  };

  handleFormShopping = (e, itemId, itemValue, itemName) => {
    e.preventDefault();

    if (this.state.assetType === undefined) {
      this.setState({
        alertShow: true,
        variant: "danger",
        shoppingMessage: "Precisa selecionar um Ativo para ser debitado"
      });
    } else {
      this.setState({
        alertShow: true,
        variant: "success",
        shoppingMessage: "Deseja realmente realizar essa compra?",
        itemPurchased: true,
        itemId,
        itemValue,
        itemName
      });
    }
  };

  handleAlertDismiss = () => {
    this.setState({
      alertShow: false,
      variant: "",
      shoppingMessage: "",
      purchaseMessage: "",
      itemId: "",
      itemValue: "",
      itemName: ""
    });
  };

  handleAlert = () => {
    const { handleAlertDismiss, handleConfirmPurchase } = this;
    if (this.state.alertShow) {
      return (
        <Alert
          variant={this.state.variant}
          onClose={handleAlertDismiss}
          dismissible
        >
          <Alert.Heading>{this.state.shoppingMessage}</Alert.Heading>
          {this.state.itemPurchased && (
            <div>
              <Button variant="success" onClick={handleConfirmPurchase}>
                Confirmar
              </Button>
              <hr />
              <p>
                Item: {this.state.itemName}
                <br />
                R$ {Number(this.state.itemValue).toFixed(2)}
              </p>
            </div>
          )}
        </Alert>
      );
    }
  };

  render() {
    const { handleAlert, ItemShoppingCard, handleAssetMaterial } = this;
    return (
      <Container>
        <Row>
          <Col>{handleAlert(this.state.shoppingMessage)}</Col>
        </Row>
        <Form>
          <Form.Group controlId="assetMaterial">
            <Dropdown style={{ margin: "10px" }}>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                Ativo a ser debitado
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onSelect={(eventKey, event) =>
                    handleAssetMaterial(eventKey, event)
                  }
                  eventKey="METAL"
                >
                  Metal
                </Dropdown.Item>
                <Dropdown.Item
                  onSelect={(eventKey, event) =>
                    handleAssetMaterial(eventKey, event)
                  }
                  eventKey="PAPEL"
                >
                  Papel
                </Dropdown.Item>
                <Dropdown.Item
                  onSelect={(eventKey, event) =>
                    handleAssetMaterial(eventKey, event)
                  }
                  eventKey="PLASTICO"
                >
                  Plástico
                </Dropdown.Item>
                <Dropdown.Item
                  onSelect={(eventKey, event) =>
                    handleAssetMaterial(eventKey, event)
                  }
                  eventKey="VIDRO"
                >
                  Vidro
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Form.Group>
          Ativo: {this.state.assetType} <br />
          Saldo:
          {this.state.assetTotal && (
            <CurrencyFormat
              value={Number(this.state.assetTotal).toFixed(2)}
              displayType={"text"}
              thousandSeparator={true}
            />
          )}
        </Form>
        <Row>{this.state.itemsForSale.map(ItemShoppingCard)}</Row>
      </Container>
    );
  }
}

export default Shopping;
